import React, { Component } from 'react';
import './App.css';

class Results extends Component {
  constructor(props) {
    super(props);
    console.log(props.data);
    this.state = {
        filter:"select",
        values:["select","Australia"]
    }
    this.handleBack = this.handleBack.bind(this);
  }
  handleChange(event)
  {
    this.setState({[event.target.name]:event.target.value}); //getting filter value
    
  }
  handleBack()
  {
      this.props.parent.setState({viewForm:true,viewResults:false}); //back to fill form
  }
  render()
  {
    return (
    <div className="Result">
    Filter Value:<select value={this.state.filter} onChange={(event) => this.handleChange(event) } name ="filter">
          {this.state.values.map((value) => <option key={value} value={value}>{value}</option>)}
          </select>
      <table id="customers">
      <tr>
          <th>Name</th>
          <th>Email</th>
          <th>Phone</th>
          <th>DOB</th>
          <th>Address</th>
          <th>City</th>
          <th>State</th>
          <th>Country</th>
      </tr>
  {this.state.filter === "select" ?<tbody>
  {this.props.data.map((person, p) => {
       return [
        <tr key={p}>
          <td>{person.name}</td>
          <td>{person.email}</td>
          <td>{person.phone}</td>
          <td>{person.dob}</td>                      {/*generating data in table*/}
          <td>{person.streetAddress}</td>
          <td>{person.city}</td>
          <td>{person.selectedState}</td>
          <td>{person.selectedCountry}</td>
        </tr> 
       ];
  })}
</tbody>:<tbody>
  {this.props.data.filter(c => c.selectedCountry === this.state.filter && c.selectedCountry !== "select").map((person, p) => {
       return [
        <tr key={p}>
          <td>{person.name}</td>
          <td>{person.email}</td>
          <td>{person.phone}</td>
          <td>{person.dob}</td>
          <td>{person.streetAddress}</td>
          <td>{person.city}</td>
          <td>{person.selectedState}</td>
          <td>{person.selectedCountry}</td>
        </tr> 
       ];
  })}
</tbody>}
  </table>
  <button type="button" className="resultBtn" onClick = {this.handleBack}>Fill Records</button>
      </div>
    );
  }
}

export default Results;
